# Generation ID tool
This tool provides a reduced version Id attribution functionality found in the Generation analysis toolkit (https://github.com/labode/genana_py). When supplied with a .dot graph and its root node label it will return a copy of the graph with unique Ids attributed to each edge (see restrictions listed below). A rendering of the graph will also be returned as a .png file.

## Requirements
Required packages are listed in requirements.txt and can be installed using pip as follows:\
`pip3 install -r requirements.txt`

## Input
- .dot graph containing nodes, edges and coordinate labels (graph **must** be a (rooted) tree i.e. may **not** contain any parallel edges/circular paths in the graph)
- root node of graph


## Output
- .dot graph with Id labels
- .png image of graph

## Usage
`python3 genid.py graph.dot label_root_node`

