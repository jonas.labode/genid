import networkx as nx
import pydot
import argparse


def read_graph(file):
    # Read graph from .dot file and turn it into a networkx graph
    # https://networkx.org/documentation/stable/reference/generated/networkx.drawing.nx_pydot.read_dot.html
    dot_graph = nx.Graph(nx.drawing.nx_pydot.read_dot(file))

    return dot_graph


def give_id(node, nx_graph):
    unique_id = 1
    visited = [node]
    nodes = [node]

    while len(nodes) != 0:
        nn = []
        for i in nodes:
            visited.append(i)
            neighbors = find_new_neighbors(i, visited, nx_graph)
            for j in neighbors:
                print("Edge between " + str(i) + " and " + str(j) + " has id " + str(unique_id))
                nx_graph[str(i)][str(j)]['Id'] = unique_id
                nn.append(j)
                unique_id += 1
        nodes = nn

    return nx_graph


def find_new_neighbors(node, known, nx_graph):
    discovered = []

    neighbors = nx.neighbors(nx_graph, node)
    for i in list(neighbors):
        if i in known:
            continue
        else:
            discovered.append(str(i))

    return discovered


def write(nx_graph, target_file, node):
    filename = str(target_file) + ".dot"
    output_file = open(filename, "w")

    output_file.write("graph G {\n")

    # Get all nodes, print those out
    nodes = nx.nodes(nx_graph)
    for i in nodes:
        output_file.write(str(i) + ";\n")

    # Write each edge pair with generation marking, hierarchically ordered
    visited = [node]
    nodes = [node]

    while len(nodes) != 0:
        nn = []
        for i in nodes:
            visited.append(i)
            neighbors = find_new_neighbors(i, visited, nx_graph)
            for j in neighbors:
                output_file.write(
                    str(i) + "--" + str(j) +
                    " [ label = \"Id " + str(nx_graph[i][j]['Id']) + "\"];\n"
                )
                nn.append(j)
        nodes = nn

    output_file.write("}")
    output_file.close()

    # Make PNG from graph
    (output_graph,) = pydot.graph_from_dot_file(filename)
    output_graph.write_png(str(target_file) + ".png")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Assign ids to .dot graph and output image')
    parser.add_argument('input_file', action='store', type=str, help='Dot file')
    parser.add_argument('root_node', action='store', type=str, help='Root node of the graph')

    args = parser.parse_args()

    input_file = args.input_file
    root_node = args.root_node

    graph = read_graph(input_file)
    graph_w_ids = give_id(root_node, graph)
    write(graph_w_ids, 'graph', root_node)

